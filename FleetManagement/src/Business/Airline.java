/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import Business.Flight.Flight;
import java.util.ArrayList;

/**
 *
 * @author Sanjeev MD
 */
public class Airline {
    
    public Airline(){
    fleets = new ArrayList<Flight>();
    }
    private String name;
    
    private ArrayList<Flight> fleets;
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public Flight addFleet(){
        Flight fleet = new Flight();
        this.fleets.add(fleet);
        return fleet;
    }
    @Override
    public String toString(){
        return this.name;
    }
    
    public int getFleetCount() {
        return fleets.size();
    }
    
    public int getTurnOver(){
        return 0;
    }

}
